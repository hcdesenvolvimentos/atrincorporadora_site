// Chat click
$( ".chat" ).click(function() {
  $zopim.livechat.window.show();
});

// SVG Fix
if (!Modernizr.svg) {
    $('img[src$=".svg"]').each(function() {
        $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });
}

// Menu Active
$(document).ready(function(){
    $('a').each(function(index) {
        if(this.href.trim() == window.location)
            $(this).addClass("active");
    });
});

//Scroll Fluid to #div
$('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash,
    $target = $(target);

    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
        window.location.hash = target;
    });
});


$(".box-diferencial").click(function() {

    if( $(this).hasClass("open") ) {
        $(".box-diferencial").removeClass("open");
    } else {
        $(".box-diferencial").addClass("open");
    }
});