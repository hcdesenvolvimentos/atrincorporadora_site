<?php
/*
	Template Name: Venda seu Terreno
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container white-area-padding">
            <div class="row row-area">
                <div class="col-md-6 interesse">
                    <div class="clear-fix">
                        <h2>Venda seu Terreno:</h2>
			<div role="main" id="atr-venda-seu-terreno-e03f4223ba60e68bad88"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
  new RDStationForms('atr-venda-seu-terreno-e03f4223ba60e68bad88-html', 'UA-86101785-4').createForm();
			</script>
                        <span class="left">*Nosso tempo médio de respostas é de 24hs.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
