<?php header('Content-Type: text/html; charset=utf-8'); ?>
<!doctype html>

<!--[if lt IE 7]>
    <html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>
    <html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]>
    <!--> <html class="no-js pt">
<!--<![endif]-->

<head>

    <!-- Charset  -->
    <meta charset="UTF-8">


    <!-- Meta  -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    

    <?php
      $thumb_id = get_post_thumbnail_id();
      $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
    ?>

    <!-- Google Authorship and Publisher Markup -->
    <link rel="author" href="https://plus.google.com/105996994881608181313/"/>
    <link rel="publisher" href="https://plus.google.com/105996994881608181313"/>

    <!-- Social: Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php wp_title( '|', true, 'right' ); ?>">
    <meta name="twitter:description" content="ATR Incorporadora">
    <?php if(is_home()) { ?>
      <meta name="twitter:image:src" content="<?php echo get_home_url(); ?>/assets/img/thumbnail.jpg">
      <meta property="og:image" content="<?php echo get_home_url(); ?>/img/social/thumbnail.jpg"/>
      <meta itemprop="image" content="<?php echo get_home_url(); ?>/img/social/thumbnail.jpg">
    <?php } else { ?>
      <meta name="twitter:image:src" content="<?php echo $thumb_url[0]; ?>">
      <meta property="og:image" content="<?php echo $thumb_url[0]; ?>"/>
      <meta itemprop="image" content="<?php echo $thumb_url[0]; ?>">
    <?php } ?>
    <!-- Social: Facebook / Open Graph -->
    <!-- <meta property="fb:admins" content="1234567890"> -->
    <meta property="og:url" content="<?php echo get_permalink(); ?>">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php wp_title( '|', true, 'right' ); ?>">
    <meta property="og:description" content="ATR Incorporadora">
    <meta property="og:site_name" content="ATR Incorporadora">

    <!-- Social: Google+ / Schema.org  -->
    <meta itemprop="name" content="<?php wp_title( '|', true, 'right' ); ?>">
    <meta itemprop="description" content="ATR Incorporadora">

    <!-- Viewport  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_home_url(); ?>/assets/img/meta/favicon.png">


    <!-- Icons iOS -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_home_url(); ?>/assets/img/meta/apple-touch-icon-152x152.png">


    <!-- Icon Windows -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_home_url(); ?>/assets/img/meta//mstile-144x144.png">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

    <!-- Libs Head -->
	<!-- Alertify -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/alertify/css/alertify.core.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/alertify/css/alertify.default.css">

	<!-- Lightbox -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/lightbox/css/lightbox.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/fontawesome/css/font-awesome.min.css">

	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/bootstrap/bootstrap.min.css">

	<!-- Bootstrap 2.3 -->
	<!-- <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap/legacy/bootstrap.min.css"> -->

	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/normalize/normalize.css">


    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo get_home_url(); ?>/assets/css/style.css">
    
    <!-- Modernizr -->
    <script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/modernizr/modernizr.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44563122-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '622929397838979']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=622929397838979&amp;ev=PixelInitialized" /></noscript>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1231936053525449');
fbq('track', "PageView");
fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1231936053525449&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</head>

<body>

<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- MENU DESKTOP -->
        <div class="logo-desktop">
          <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_home_url(); ?>/assets/img/logo.svg" alt="Logo ATR Brazil"></a>
        </div>
        <div id="menu-desktop">
          <?php wp_nav_menu( array('menu' => 'Header Menu', 'items_wrap' => '<ul>%3$s</ul>' )); ?>
        </div>
        <div id="menu-support">
          <div class="fone"><span>(41) </span>3308.0402</div>
          <ul>
            <li><a href="<?php echo get_home_url(); ?>/contato" class="email">Email</a></li>
            <li><a href="javascript:void(0)" class="chat">Chat Online</a></li>
            <li><a data-toggle="modal" data-target="#whatsapp-modal" href="#" class="whatsapp">Whatsapp</a></li>
            <li><a href="https://www.facebook.com/atrincorporadora" target="_blank" class="facebook">Siga-nos</a></li>
          </ul>
        </div>

        <!-- MENU MOBILE -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation" id="menu-mobile">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Alternar Navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="home"><img src="<?php echo get_home_url(); ?>/assets/img/logo.svg" alt="Logo ATR Brazil" width="50"></a>
            </div>
            <nav class="navbar-collapse collapse">
              <?php wp_nav_menu( array('menu' => 'Header Menu', 'items_wrap' => '<ul class="nav navbar-nav navbar-right">%3$s</ul>' )); ?>
            </nav><!--/.nav-collapse -->
          </div>
        </div>

      </div>
    </div>
  </div>
</header>

<?php 
  if( !is_home() ) {
?>

    <div class="row row-area breadscrumb-row">
      <div class="container">
        
        <div class="col-md-12">
          <div id="bread-crumb">
            <div class="breadcrumbs">
                <h1><span>> </span><?php the_title(); ?></h1>
                <?php if(function_exists('bcn_display'))
                {
                    bcn_display();
                }?>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php 
  }
?>