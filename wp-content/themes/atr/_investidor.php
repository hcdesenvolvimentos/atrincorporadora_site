<?php
/*
	Template Name: Investidor
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container white-area-padding">
            <div class="row row-area">
                <div class="col-md-6">
                    <div class="clear-fix">
                        <h2>Investidor:</h2>
                        <p>O mercado imobiliário sempre foi um dos principais destinos de investimento de pessoas físicas, isso se deve a alta rentabilidade do setor e da possibilidade a garantia do bem imóvel oferecido pela construtora.</p>
                        <p>Esse cenário aliado a liquidez que a ATR alcança com seus produtos, reconhecidos como de elevada qualidade, significa também uma ótima opção de investimento.</p>
                        <p>Trabalhamos com remunerações aproximadas entre 17% e 25%, com alta segurança e fácil liquidez.</p>
                        <p>Não se trata da compra de imóvel na planta, que é sim uma ótima opção de investimento, mas traz consigo o ônus da venda ou locação.</p>
                        <p>Se tornando um sócio-investidor da ATR, você não se torna dono de uma unidade especificamente, mas sim tem sua rentabilidade garantida em contrato com tempo pré-determinado para encerramento e recebimento. <b>Sem risco para unidades não vendidas, sem risco para atraso de obras.</b></p>
                        <p>Ao final do contrato, que aliena um ou mais unidades como garantia, você liquida o investimento realizando os lucros auferidos. A responsabilidade e risco da venda e construção são 100% da ATR, e você acompanha tudo através de relatório mensais.</p>
                    </div>
                </div>
                <div class="col-md-6 interesse">
                    <div class="clear-fix">
                       <div role="main" id="atr-investidor-c9f1fcdeb55a8efdd8be"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
  new RDStationForms('atr-investidor-c9f1fcdeb55a8efdd8be-html', 'UA-86101785-4').createForm();
			</script>
                        <span class="left">*Nosso tempo médio de respostas é de 24hs.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
