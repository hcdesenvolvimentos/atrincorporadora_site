<?php
/*
	Template Name: Empreendimentos
*/

get_header(); ?>


    <div id="dad">
        <!-- Page Content -->
        <div class="container">
            <div class="row row-area">
                <div class="col-md-12">

                    <?php
                        query_posts( array(
                              'post_type' => 'bannerempreendimento',
                              'posts_per_page' => 1,
                              'orderby'=> 'date&order=ASC',
                              'post__not_in' => array($post_ID)
                         ));
                    ?>
                    <?php 
                        if ( have_posts() ) :
                            while ( have_posts() ) : the_post();
                    ?>
                        <img src="<?php the_field('bannerempreendimento'); ?>" height="391" width="1135" alt="" class="banner-empreendimentos">
                        <img class="home-shadow" src="<?php echo get_home_url(); ?>/assets/img/shadow-slide.png" alt="">
                    <?php
                            endwhile;
                            wp_reset_query();
                            // // Previous/next post navigation.
                            // twentyfourteen_paging_nav();
                        else :
                    ?>
                        <p>Nenhum Empreendimento foi Encontrato</p>
                    <?php
                        endif;
                    ?>
                </div>
                <div class="col-md-12">
                    <h2 class="red-info">Encontre o empreendimento que mais combina com você:</h2>
                </div>

                <?php
                    query_posts( array(
                          'post_type' => 'apartamentos',
                          'posts_per_page' => 999,
                          'orderby'=> 'date&order=ASC',
                          'post__not_in' => array($post_ID)
                     ));
                ?>
                <?php 
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                ?>


                <div class="col-md-4">
                    <a href="<?php the_permalink(); ?>">
                        <div class="empreendimento-box">
                            <div class="thumb">
                                <?php the_post_thumbnail(); ?>
                                <?php 
                                    $status = explode(':', get_field('status'));
                                 ?>
                                <span class="empreendimento-status <?php echo $status[0]; ?>"><span><?php echo $status[1]; ?></span></span>
                            </div>
                            <h2><?php the_title(); ?></h2>
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?php the_permalink(); ?>" class="read-more">Saiba +</a>
                        </div>
                    </a>
                </div>

                <?php
                        endwhile;
                        wp_reset_query();
                        // // Previous/next post navigation.
                        // twentyfourteen_paging_nav();
                    else :
                ?>
                    <p>Nenhum Empreendimento foi Encontrato</p>
                <?php
                    endif;
                ?>
            </div>
        </div>
    </div>

<?php
get_footer();
