<footer>
	<div class="container">
		<div class="row row-area">
			<div class="col-md-3 contact-banner">
				<a href="contato"><img src="<?php echo get_home_url(); ?>/assets/img/contato-banner.png" height="154" width="245" alt=""></a>
			</div>
			<div class="col-md9">
	        <div id="menu-support">
	          <ul>
	            <li><a href="<?php echo get_home_url(); ?>/contato" class="email">Email</a></li>
	            <li><a href="javascript:void(0)" class="chat">Chat Online</a></li>
	            <li><a href="#" data-toggle="modal" data-target="#whatsapp-modal" class="whatsapp">Whatsapp</a></li>
	            <li><a href="https://www.facebook.com/atrincorporadora" target="_blank" class="facebook">Siga-nos</a></li>
            	<li><a href="http://app.vc/app_atr" target="_blank" class="app">Aplicativo ATR</a></li>
            	<li class="phone"><span class="phone-number"><span class="ddd">(41)</span> 3308.0402</span></li>
	          </ul>
	        </div>
			</div>
		</div>
		<div class="row row-area">
			<div class="col-md-12">
				<div id="bread-crumb">
					<div class="breadcrumbs">

					</div>
				</div>
			</div>
		</div>
		<div class="row row-area">
			<div class="col-md-4 footer-block">
				<h3>Redes Sociais</h3>
				<div class="text-block">
					<a href="https://www.facebook.com/atrincorporadora" target="_blank"><img src="<?php echo get_home_url(); ?>/assets/img/facebook-mini-ico.png" height="25" width="25" alt="Facebook ATR"></a>
					<a href="https://plus.google.com/105996994881608181313/" target="_blank"><img src="<?php echo get_home_url(); ?>/assets/img/gplus-mini-ico.png" height="25" width="25" alt="Google Plus ATR"></a>
				</div>
				<div class="text-block">
					<p>VISITE NOSSO PLANTÃO DE VENDAS</p>
					<p>Seg a Sex das 08:00 as 18:00</p>
					
				</div>
				<div class="text-block">
					<p>ATR INCORPORADORA</p>
					<p>TEL: (41) 3308.0402</p>
					<p>CEL: (41) 7400.9585</p>
				</div>
				<div class="text-block">
					<p>R. Francisco Toczek, 579</p>
					<p>Bairro Afonso Pena</p>
					<p>São José dos Pinhais, PR</p>
				</div>
				<div class="text-block">
					<p>CNPJ: 19.161.749/0001-49</p>
				</div>
			</div>
			<div class="col-md-4 footer-block">
				<h3>Fale Conosco</h3>
                <?php echo do_shortcode( '[contact-form-7 id="87" title="Contato"]' ); ?>
			</div>
			<div class="col-md-4 footer-block">
				<h3>Mapa do Site</h3>
				<ul class="mapa-site">
					<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
					<li><a href="<?php echo get_home_url(); ?>/sobre">Sobre</a></li>
					<li><a href="<?php echo get_home_url(); ?>/diferenciais">Diferenciais</a></li>
					<li><a href="<?php echo get_home_url(); ?>/empreendimentos">Empreendimentos</a></li>
					<ul>
		                <?php
		                    query_posts( array(
		                          'post_type' => 'apartamentos',
		                          'posts_per_page' => 999,
		                          'orderby'=> 'date&order=ASC',
		                          'post__not_in' => array($post_ID)
		                     ));
		                ?>
		                <?php 
		                    if ( have_posts() ) :
		                        while ( have_posts() ) : the_post();
		                ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

		                <?php
		                        endwhile;
		                        wp_reset_query();
		                        // // Previous/next post navigation.
		                        // twentyfourteen_paging_nav();
		                    else :
		                ?>
		                    <p>Nenhum Empreendimento foi Encontrato</p>
		                <?php
		                    endif;
		                ?>
					</ul>
					<li><a href="<?php echo get_home_url(); ?>/news">News</a></li>
					<li><a href="<?php echo get_home_url(); ?>/contato">Contato</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid footer-bar">
		<div class="row">
			<div class="col-md-12">
				<div class="footer-bar-content">
					<div>
						<img src="<?php echo get_home_url(); ?>/assets/img/logo-grey.svg" alt="" width="49" height="69">
					</div>
					<span>Rua Francisco Toczek, 579 - São José dos Pinhais</span>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- whatsapp modal -->
<div class="modal fade equipe-modal" id="whatsapp-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 class="modal-title" id="myModalLabel">Whatsapp ATR</h2>
      </div>
      <div class="modal-body">
        <div class="avatar-photo">
            <!-- <img src="assets/img/upload/avatar-losango.png" height="200" width="201" alt=""> -->
        </div>
        <p>Quer saber mais?</p>
        <p>Mande um Whatsapp para <b>O SUISSA EL MESTRE</b></p>
      </div>
    </div>
  </div>
</div>


<!-- jQuery Plugins -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/jquery/jquery-1.11.1.min.js"></script>

<!-- Prefix Free -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/prefixfree/prefixfree.min.js"></script>

<!-- Alertify -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/alertify/js/alertify.js"></script>

<!-- Lightbox -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/lightbox/js/lightbox.min.js"></script>

<!-- Lince Form -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/linceform/linceform.js"></script>

<!-- Bootstrap -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/bootstrap/bootstrap.min.js"></script>

<!-- Bootstrap 2.3 -->
<!-- <script type="text/javascript" src="assets/libs/bootstrap/legacy/bootstrap.min.js"></script> -->

<!--  Cycle 2  -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/cycle2/jquery.cycle2.min.js"></script>

<!-- Main JS -->
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/js/app.js"></script>


<!-- Google Analytics -->


<!-- Facebook API -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>