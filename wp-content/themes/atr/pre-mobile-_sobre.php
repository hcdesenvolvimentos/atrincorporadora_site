<?php
/*
	Template Name: Sobre
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 white-area-padding">
                    <div class="clear-fix page-block">
                        <div class="video-sobre">
                            <iframe width="650" height="420" src="//www.youtube.com/embed/1TCkRL6-dis" frameborder="0" allowfullscreen></iframe>
                            <img class="home-shadow" src="<?php echo get_home_url(); ?>/assets/img/shadow-slide.png" alt="">
                        </div>
                        <h2 class="ico-logo-up">A ATR atua no negócio de moradia. Fazemos as pessoas conseguirem o lar que sempre sonharam. Somos especializados em:</h2>
                                <ul class="about-list">
                                    <li>Seleção de Propriedades </li>
                                    <li>Gerenciamento de projetos</li>
                                    <li>Gestão da qualidade</li>
                                    <li>Vendas e Marketing </li>
                                </ul>
                                <p>Nosso nicho de mercado são jovens buscando seu primeiro apartamento em São José dos Pinhais.</p>
                    </div>
                    <div class="page-block">
                        <h2 class="ico-logo-up">Pensamos em todos os Detalhes do seu imóvel.</h2>
                        <p>Estamos sempre atentos às inovações tecnológicas que podem facilitar a sua vida e trabalhamos com empresas certificadas e marcas de boa qualidade. Temos uma equipe de arquitetos criativos e de bom gosto, lançando apenas empreendimentos diferentes do “quadrado”.</p>
                        <p>Nossa estrutura dinâmica garante que ATR Incorporadora seja capaz de aproveitar as oportunidades de forma rápida e possa ajustar as estratégias para um mercado em constante mudança, o que nos permite estar na vanguarda do nosso setor de desenvolvimento.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-7 page-block">
                    <h4>A este ponto você já deve ter percebido:</h4>
                    <p>Somos uma empresa movida pela inovação!  Estamos presentes nas principais feiras e eventos do setor imobiliário com foco em trazer os melhores benefícios, por isso conseguimos oferecer a solução completa para você que está procurando os melhores apartamentos!</p>
                </div>
                <div class="col-md-5 page-block shadow-div">
                    <a href="<?php echo get_home_url(); ?>/apartamentos" class="link-logo-bot">
                        Começe agora, conheça nossos empreendimentos
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
