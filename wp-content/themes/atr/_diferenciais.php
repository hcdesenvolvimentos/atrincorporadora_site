<?php
/*
	Template Name: Diferenciais
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-diferencial bg-blue-sky">
                        <h2>Plantas Flexíveis</h2>
                        <div class="resumo">
                            <p>Resolveu aumentar a família?</p>
                            <p>Sem problema ajustamos sua planta.</p>
                        </div>
                        <div class="content-dif">
                            <p>Seu apartamento, suas escolhas.</p>
                            <br>
                            <p class="white">Prefere quartos maiores ou uma sala mais ampla?
                            Quer seu apartamento ATR com 1, 2 ou 3 dormitórios?</p>
                            <p class="white">Você pode escolher!</p>
                            <br>
                            <p>Você pode, por exemplo, comprar um apartamento de 2 quartos e caso a família aumente, alterar para 3 quartos.</p>
                            <br>
                            <p class="white">Seu apartamento ainda mais exclusivo e personalizado para suas necessidades. Com o novo conceito de flexibilização da ATR você pode escolher <u>até 3 opções de planta no mesmo apartamento.</u></p>
                            <br>
                            <p>E sem custo extra por isso!</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-diferencial bg-blue-dark">
                        <h2>Entrada parcelada em até 12x</h2>
                        <div class="resumo">
                            <p>Aquela ajuda inicial?</p>
                            <p>A Atr flexibiliza sua entrada.</p>
                        </div>
                        <div class="content-dif">
                            <p>Fizemos centenas de simulações em 2014, nós sabemos, a entrada ainda é uma das maiores dificudades na hora de comprar um apartamento.</p>
                            <br>
                            <p class="white">Por isso a ATR da uma ajuda inicial para você! Parcele o valor da entrada* em até 12x sem juros!</p>
                            <br>
                            <p class="verysmall">*Válido para 10% do valor do apartamento</p>
                            <p class="verysmall">Pagamento antes e depoisda entrega das chaves!</p>
                            <br>
                            <p>Tudo isso com a melhor e mais justa negociação do mercado:</p>
                            <br>
                            <p>• Sem juros de obra</p>
                            <p>• Sem correção pelo INCC</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-diferencial bg-white">
                        <h2>Opção de vaga dupla</h2>
                        <div class="resumo">
                            <p>Mais de um automóvel?</p>
                            <p>Pensamos nisso também.</p>
                        </div>
                        <div class="content-dif">
                            <p><b>Mais de um carro na família?</b></p>
                            <p>Nós pensamos nisso também!</p>
                            <br>
                            <p><b>Nos esforçamos para oferecer a opção de 2 vagas no chão em todos empreendimentos.</b></p>
                            <br>
                            <p>Mas além disto, em parceria com a empresa Garage Plan, desenvolvemos outro diferencial:</p>
                            <p>Duplicador de Vagas</p>
                            <div>
                                <div class="left-garage">
                                    <p class="verysmall">SEGURO, DURÁVEL E VERSÁTIL</p>
                                    <img src="<?php echo get_home_url(); ?>/assets/img/upload/garage-plan.jpg" alt="Garage Plan">
                                </div>
                                <div class="left-garage">
                                    <p>• Sistema de acionamento rápido e simples</p>
                                    <p>• Sistema de proteção contra vazamentos</p>
                                    <p>• Sistema hidráulico libera a vaga em menos de 1 minuto</p>
                                    <p><b>www.garageplan.com.br</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 nomobile-content">
                    <h2 class="red-info red-dif">Confira também alguns dos diferenciais  que você vai encontrar em nossos empreendimentos.</h2>
                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box fechadura">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb.jpg" height="105" width="105" alt="">
                        </div>
                        <h3>FECHADURA <b>BIOMÉTRICA</b></h3>
                        <p>Você já imaginou entrar e sair de casa sem precisar de chaves? Os apartamentos ATR contam com tecnologia biométrica, a fechadura pode ser aberta com a chave, mas se você esquecer (ou simplesmente preferir não usar) a porta da rua pode ser aberta somente através de reconhecimento da sua impressão digital.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box aquecimento">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb-aquecimento.jpg" height="105" width="105" alt="">
                        </div>
                        <h3><b>Aquecimento</b> a Gás</h3>
                        <p>Os apartamentos ATR contam com central de abastecimento de gás GLP. Isso significa praticidade e economia para utilização do fogão e ainda proporciona o conforto da agua quente oferecida pelo sistema de aquecimento* com distribuição para o chuveiro.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box pisoquente">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb-piso.jpg" height="105" width="105" alt="">
                        </div>
                        <h3>PISO <b>AQUECIDO</b></h3>
                        <p>O seu apartamento com piso aquecido é muito mais confortável:</p>
                        <ul>
                            <li>Aquece rápido todo o ambiente 6°C em 1h;</li>
                            <li>38% mais econômico que ar condicionado;</li>
                            <li>Perfeito para quem tem alergia;</li>
                            <li>Sem ruído, nem cheiro;</li>
                            <li>Manutenção zero;</li>
                            <li>50 anos de Garantia;</li>
                        </ul>
                        <a href="http://www.eurocablebrasil.com.br">www.eurocablebrasil.com.br</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box arcond">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb-arcon.jpg" height="105" width="105" alt="">
                        </div>
                        <h3>PREPARAÇÃO PARA <b>AR CONDICIONADO SPLIT</b></h3>
                        <p>O seu apartamento com a temperatura certa. Você recebe seu apartamento ATR com toda organização prévida das tubulações, drenos e cabos de espera para ar condicionado. Oferecendo a opção ágil e barata na hora de instalar o equipamento, sem que seja necessária qualquer intervenção na alvenaria, elétrica ou acabamentos</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box cftv">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb-cftv.jpg" height="105" width="105" alt="">
                        </div>
                        <h3><b>CFTV</b></h3>
                        <p>Segurança é fundamental. Por isso os apartamentos ATR contam com CFTV (Circuito Fechado de Televisão). Câmeras instaladas em pontos estratégicos gravam e armazenam por 60 dias as imagens, podendo as cameras serem acessadas inclusive pelo seu celular quando estiver fora de casa!</p>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="diferenciais-box bicicletas">
                        <div class="thumb">
                            <img src="<?php echo get_home_url(); ?>/assets/img/upload/diferenciais-thumb-bike.jpg" height="105" width="105" alt="">
                        </div>
                        <h3>VAGAS PARA <b>BICICLETAS</b></h3>
                        <p>A ATR tem compromisso com a mobilidade urbana, por isso para cada vaga de carro oferece uma vaga para bicicleta. Os espaços para bicicletas são projetados pela Bike Fácil, a empresa é única no Brasil especializada em bicicletários e o projeto leva em consideração a ergonomia, praticidade e estética do local destinado a proteger as bikes.</p>
                        <a href="http://www.bicicletarios.com.br">www.bicicletarios.com.br</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
