<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php 
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
?>
<?php 
        if(get_post_type() == "apartamentos") {
 ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container contact">
            <div class="row row-area">
                <div class="col-md-3 page-block">
                    <div class="empreendimento-map">
                        <h1 class="acesso-direto">Acesso direto</h1>
                        <div>
                            <a href="#midia">Vídeos & Fotos</a>
                        </div>
                        <div>
                            <a href="#mapa">Mapa</a>
                        </div>
                        <div>
                            <a href="#plantas">Plantas</a>
                        </div>
                        <div>
                            <a href="#diferenciais">Diferenciais</a>
                        </div>
                        <div>
                            <a href="#motivos">Por que escolher este imóvel?</a>
                        </div>
                        <div>
                            <a href="#simulacao">Solicite uma simulação</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 emp-dad page-block">
                    <div class="row">
                        <div class="row-emp clear">
                            <div class="col-md-12">
                                <img src="<?php the_field('banner_topo'); ?>" alt="" class="banner-empreendimento nomobile-content">
                                <img src="<?php the_field('banner_topo_mobile'); ?>" alt="" class="banner-empreendimento mobile-content">
                            </div>
                            <img class="home-shadow" src="<?php echo get_home_url(); ?>/assets/img/shadow-slide.png" alt="">
                        </div>
                        <div class="clear">
                            <div class="col-md-12 content-post">
                                <?php 
                                    $status = explode(':', get_field('status'));
                                 ?>
                                <span class="empreendimento-status <?php echo $status[0]; ?>"><span><?php echo $status[1]; ?></span></span>
                            </div>
                        </div>
                        <!-- <div class="row-emp clear">
                            <div class="col-md-12 content-post">
                                <?php the_content(); ?>
                            </div>
                        </div> -->
                        <div class="row-emp clear">
                            <div class="col-md-6 block-emp video-embed"  id="midia">
                                <h2>Vídeo</h2>
                                <?php the_field('link_do_video'); ?>
                            </div>
                            <div class="col-md-6 block-emp">
                                <h2>Imagens</h2>
                                <div class="img-block">
                                    <?php if(get_field('imagens')): ?>
                                        <?php while(the_repeater_field('imagens')): ?>
                                            <?php $image = wp_get_attachment_image_src(get_sub_field('imagem'), 'full'); ?>
                                            <?php $thumb = wp_get_attachment_image_src(get_sub_field('imagem'), 'thumbnail'); ?>
                                            <a href="<?php echo $image[0]; ?>" data-lightbox="galeria-imagens">
                                                <img src="<?php echo $thumb[0]; ?>" height="832" width="800" alt="<?php the_sub_field('titulo_imagem');?>">
                                            </a>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-emp clear">
                            <div class="col-md-6 block-emp gmaps" id="mapa">
                                <h2>Mapa</h2>
                                <?php the_field('link_gmaps'); ?>
                            </div>
                            <div class="col-md-6 block-emp gmaps" style="position: relative;">
                                <!-- <div style="width: 100%; height: 100%; z-index: 9999; position: absolute;"></div> -->
                                <h2>Veja o local</h2>
                                <?php the_field('link_street_view'); ?>
                            </div>
                        </div>
                        <div class="row-emp clear">
                            <div class="col-md-12" id="plantas">
                                <h2 class="hcenter">Plantas</h2>
                                <div class="planta">
                                    <div id="slider-container"
                                         data-cycle-pause-on-hover="true"
                                         class="cycle-slideshow"
                                         data-cycle-fx="fade"
                                         data-cycle-timeout="0"
                                         data-cycle-slides="> a"
                                          data-cycle-pager=".planta-nav" >

                                        <?php if(get_field('plantas')): ?>
                                            <?php while(the_repeater_field('plantas')): ?>
                                                <?php $image = wp_get_attachment_image_src(get_sub_field('imagem_da_planta'), 'full'); ?>
                                                <?php $thumb = wp_get_attachment_image_src(get_sub_field('imagem_da_planta'), 'large-thumb'); ?>
                                                <a class="plant-element-sl" href="<?php echo $image[0]; ?>"  data-lightbox="galeria-plantas" data-cycle-pager-template="<a href='<?php echo $image[0]; ?>'><?php the_sub_field('nome_da_planta');?></a>">
                                                    <img src="<?php echo $thumb[0]; ?>" height="622" width="521" alt="<?php the_sub_field('nome_da_planta');?>">
                                                </a>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>

                                    <a href="#">
                                    </a>
                                </div>
                                <div class="planta-right">
                                    <div class="planta-nav" style="min-height:540px;height:auto;"></div>
                                    <a href="<?php the_field('link_pdf_plantas'); ?>" class="planta-download nomobile-content" target="_blank">Download da Planta em PDF</a>
                                </div>
                            </div>
                        </div>
                        <div class="row-emp clear">
                            <div class="" id="diferenciais">
                                <h2 class="hcenter">Diferenciais Atr</h2>

                                    <?php if(get_field('diferenciais')): ?>
                                        <?php while(the_repeater_field('diferenciais')): ?>
                                            <?php $thumb = wp_get_attachment_image_src(get_sub_field('diferencial_imagem'), 'thumbnail'); ?>
                                            <div class="col-md-4 emp-diferenciais">
                                                <div class="diferenciais-box vertical-block">
                                                    <div class="thumb">
                                                        <img src="<?php echo $thumb[0]; ?>" height="105" width="105" alt="<?php the_sub_field('diferencial_titulo');?>">
                                                    </div>
                                                    <div class="vertical-centered diferencial-title">
                                                        <h3><?php the_sub_field('diferencial_titulo');?></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                            </div>
                        </div>
                        <div class="row-emp clear">
                            <div class="" id="motivos">
                                <h2 class="hcenter">Porque escolher este imóvel:</h2>

                                    <?php if(get_field('motivos')): ?>
                                        <?php while(the_repeater_field('motivos')): ?>
                                            <?php $thumb = wp_get_attachment_image_src(get_sub_field('motivo_icone'), 'thumbnail'); ?>
                                            <div class="col-md-4 pq-escolher">
                                                <span style="background:url('<?php echo $thumb[0]; ?>') left center no-repeat;"><?php the_sub_field('motivo_titulo');?></span>
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                            </div>
                        </div>
                        <div class="row-emp clear">
                            <div class="col-md-6" id="simulacao">
                                <h2 class="hcenter">Solicite uma simulação</h2>
                              <div role="main" id="atr-simulacao-62846573a0120a1674f6"></div>
				<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
				<script type="text/javascript">
  new RDStationForms('atr-simulacao-62846573a0120a1674f6-html', 'UA-86101785-4').createForm();
				</script>
                            </div>
                            <div class="col-md-6 emp-veja-tbm">
                                <h3>Veja também</h3>
                                <div class="links">
                                    <a href="<?php echo get_home_url(); ?>/apartamentos" class="border-box">Voltar para apartamentos</a>
                                    <br>
                                    <a href="#" class="border-box">Acompanhe sua obra</a>
                                </div>
                                <div class="download-app">
                                    <a href="http://app.vc/app_atr" target="_blank">
                                        <img src="<?php echo get_home_url(); ?>/assets/img/upload/qr-code.jpg" height="100" width="100" alt="">
                                        Faça o Download do nosso Aplicativo e tenha acesso a tabela de preço sempre atualizada!
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    } else {
 ?>
     <div id="dad">
        <!-- Page Content -->
        <div class="container contact">
            <div class="row row-area">
                <div class="col-md-12">
                    <div class="post post-interna white-area-padding">
                        <h3><?php the_title(); ?></h3>
                        <div class="fb-share-button fb-solto" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                        <br>
                        <div class="fb-share-button fb-solto" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    }
 ?>
<?php
        endwhile;
    else :
?>
    <p>Nenhum conteúdo foi encontrado</p>
<?php
    endif;
?>


<?php
get_footer();
?>

<!-- Page Exclusive JS -->
<script type="text/javascript">
    jQuery(window).scroll(function () {
        if($( window ).width() > 992) {
            // Travar Menu
            var empDatHeight = $(".emp-dad").offset().top + $(".emp-dad").height();
            empDatHeight -= $(".empreendimento-map").height();
            if (jQuery(this).scrollTop() >= ($(".emp-dad").offset().top)) {
                $(".empreendimento-map").addClass("fixed");
            } else {
                $(".empreendimento-map").removeClass("fixed");
            }
            if (jQuery(this).scrollTop() >= empDatHeight ) {
                $(".empreendimento-map").addClass("invisible");
            } else {
                $(".empreendimento-map").removeClass("invisible");
            }
        } else {
            $(".empreendimento-map").removeClass("fixed");
        }
    });
</script>



<!-- Custom Scroll -->
<link rel="stylesheet" type="text/css" href="<?php echo get_home_url(); ?>/assets/libs/customscroll/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="<?php echo get_home_url(); ?>/assets/libs/customscroll/jquery.mCustomScrollbar.js"></script>
<script>
    (function($){
        $(window).load(function(){
            $(".img-block").mCustomScrollbar();
        });
    })(jQuery);
</script>
<!-- Custom Google Tracker -->
<script>
    $(document).ready(function() {
        $("#simulacao input[type='submit']").attr("onclick","ga('send', 'event','FormStep2', 'Empreendimento')");
    })
</script>
