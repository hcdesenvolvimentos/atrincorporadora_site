<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

     <div id="dad">
        <!-- Page Content -->
        <div class="container contact">
            <div class="row row-area">
                <div class="col-md-12">
                    <div class="post post-interna white-area-padding">
                        <h3>404</h3>
                        <div class="content">
                            Nenhum conteúdo foi encontrado
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
?>

