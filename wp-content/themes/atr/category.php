<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container contact">
            <div class="row row-area">
                <div class="col-md-2 blog-roll-map-lasts">
                    <div class="blog-roll-map">
                        <h1>CONFIRA AS ÚLTIMAS NOVIDADES DA ATR</h1>
                    </div>
                   <div class="blog-roll-map">
                        <h2><a href="<?php echo get_home_url(); ?>/news/imprensa">Imprensa</a></h2>

                        <?php
                            query_posts( array(
                               'posts_per_page' => 3,
                               'cat' => '3',
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>

                            <a class="link-post" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>


                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                            <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="blog-roll category-read">
                        <h2><?php single_cat_title( '', true ); ?></h2>

			            <?php 
			                if ( have_posts() ) :
			                    while ( have_posts() ) : the_post();
			            ?>

                            <div class="post" id="post-<?php echo get_the_ID(); ?>">
                                <a href="<?php the_permalink(); ?>">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="thumb">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </a>
                                <div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                            </div>

			            <?php
			                    endwhile;
			                    // // Previous/next post navigation.
			                    twentyfourteen_paging_nav();
			                else :
			            ?>
			                <p>Nenhum Post foi Encontrato</p>
			            <?php
			                endif;
			            ?>
                    </div>


                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
