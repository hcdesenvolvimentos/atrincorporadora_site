<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


    <div id="dad">
        <!-- Page Content -->
        <div class="container">
            <div class="row row-area">
                <div class="col-md-12">
                    <div id="slider">
                        <div id="slider-container"
                             data-cycle-pause-on-hover="true"
                             class="cycle-slideshow"
                             data-cycle-fx="fade"
                             data-cycle-timeout="5000"
                             data-cycle-slides="> a"
                             data-cycle-pager=".slider-pager" >
                            <?php
                                query_posts( array(
                                      'post_type' => 'sliderhome',
                                      'posts_per_page' => 5,
                                      'orderby'=> 'date&order=ASC',
                                      'post__not_in' => array($post_ID)
                                 ));
                            ?>
                            <?php 
                                if ( have_posts() ) :
                                    while ( have_posts() ) : the_post();
                            ?>
                                <a href="<?php the_field('linkbannerhome'); ?>">
                                    <img src="<?php the_field('bannerhome'); ?>" height="391" width="1135" alt="<?php the_title(); ?>">
                                </a>
                            <?php
                                    endwhile;
                                    wp_reset_query();
                                    // // Previous/next post navigation.
                                    // twentyfourteen_paging_nav();
                                else :
                            ?>
                                <p>Nenhum Slide foi Encontrato</p>
                            <?php
                                endif;
                            ?>
                        </div>
                        <div class="slider-pager"></div>
                    </div>
                    <img class="home-shadow" src="<?php echo get_home_url(); ?>/assets/img/shadow-slide.png" alt="">
                </div>
            </div>
            <div class="row row-area white-area">
                <div class="col-md-1"></div>
                <div class="col-md-4 block-home">
                    <h2 class="ico-logo">Empreendimentos</h2>
                    <div class="emp-thumb-content">
                    <?php
                        query_posts( array(
                              'post_type' => 'apartamentos',
                              'posts_per_page' => 2,
                              'orderby'=> 'date&order=ASC',
                              'post__not_in' => array($post_ID)
                         ));
                    ?>
                    <?php 
                        if ( have_posts() ) :
                            while ( have_posts() ) : the_post();
                    ?>

                        <a href="<?php the_permalink(); ?>" class="empreendimento-thumb">

                            <div class="thumb status-home">
                                <?php 
                                    $status = explode(':', get_field('status'));
                                 ?>
                                <span class="empreendimento-status <?php echo $status[0]; ?>"><span><?php echo $status[1]; ?></span></span>
                            </div>
                            <?php the_post_thumbnail( 'thumbnail' ); ?>
                        </a>

                    <?php
                            endwhile;
                            wp_reset_query();
                            // // Previous/next post navigation.
                            // twentyfourteen_paging_nav();
                        else :
                    ?>
                        <p>Nenhum Notícia foi Encontrata</p>
                    <?php
                        endif;
                    ?>
                        <a href="apartamentos" class="link-ver-todos">Conheça todas as unidades disponíveis</a>
                    </div>
                </div>
                <div class="col-md-1" style="height:50px;"></div>
                <div class="col-md-5 block-home">
                    <h2 class="ico-logo">News</h2>
                        <?php
                            query_posts( array(
                               'posts_per_page' => 1,
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>
                    <div class="news-thumb">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="news-excerpt">
                        <p><?php the_excerpt(); ?></p>
                        <a href="news" class="read-more">Saiba mais...</a>
                    </div>

                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                                <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

<?php
get_footer();
