<?php
/*
	Template Name: Contato
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container contact white-area-padding">
            <div class="row row-area">
                <div class="col-md-6">
                    <div class="row-area">
                        <h3>ATR Incorporadora</h3>
                        <p>CEL: (41) 9 8854-6447​⁠​</p>
                        <p>contato@atrincorporadora.com.br</p>
                    </div>
                    <div class="row-area">
                        <h3>Visite nosso plantão de vendas.</h3>
                        <p>R. Francisco Toczek, 579</p>
                        <p>Bairro Afonso Pena</p>
                        <p>São José dos Pinhais, PR</p>
                        <p>Seg a Sex das 08:00 as 18:00</p>
                       
                    </div>
                    <div class="row-area gmaps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3600.9040903422806!2d-49.19225199999999!3d-25.508244999999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcf08f6867ef91%3A0xd7c403462410d401!2sATR+Incorporadora+Imobili%C3%A1ria!5e0!3m2!1spt-BR!2sbr!4v1416835287392" class="gmaps" frameborder="0" style="border:0"></iframe>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5 interesse">
                    <div class="clear-fix">
                        <h2>Entre em contato:</h2>
                        <div role="main" id="atr-contato-6f13505a74a773ed4ddd"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
  new RDStationForms('atr-contato-6f13505a74a773ed4ddd-html', 'UA-86101785-4').createForm();
			</script>
                        <span class="left">*Nosso tempo médio de respostas é de 24hs.</span>
                    </div>
                    <h2>Veja Também</h2>
                    <a href="http://www.atrincorporadora.com.br/venda-seu-terreno/" class="border-box">Venda seu terreno</a>
                    <a href="http://www.atrincorporadora.com.br/investidor" class="border-box">Investidores</a>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
