<?php
/*
	Template Name: News
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container contact">
            <div class="row row-area">
                <div class="col-md-2 blog-roll-map-lasts">
                    <div class="blog-roll-map">
                        <h1>CONFIRA AS ÚLTIMAS NOVIDADES DA ATR</h1>
                    </div>
                    <div class="blog-roll-map">
                        <h2><a href="<?php echo get_home_url(); ?>/news/imprensa">Imprensa</a></h2>

                        <?php
                            query_posts( array(
                               //'posts_per_page' => 3,
                               'cat' => '3',
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>

                            <a class="link-post" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>


                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                            <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>
                    </div>
                    <div class="blog-roll-map">
                        <h2><a href="<?php echo get_home_url(); ?>/news/institucionais">Institucionais</a></h2>

                        <?php
                            query_posts( array(
                               //'posts_per_page' => 3,
                               'cat' => '1',
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>

                            <a class="link-post" href="#post-<?php echo get_the_ID(); ?>"><?php the_title(); ?></a>


                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                            <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="blog-roll">
                        <h2><a href="<?php echo get_home_url(); ?>/news/imprensa">Imprensa</a></h2>

                        <?php
                            query_posts( array(
                               //'posts_per_page' => 3,
                               'cat' => '3',
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>

                            <div class="post" id="post-<?php echo get_the_ID(); ?>">
                                <a href="<?php the_permalink(); ?>">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="thumb">
                                        <?php the_post_thumbnail( 'medium' ); ?>
                                    </div>
                                    <div class="content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </a>
                                <div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                            </div>

                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                            <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>

                    </div>
                </div>
                <div class="col-md-5">
                    <div class="blog-roll">
                        <h2><a href="<?php echo get_home_url(); ?>/news/institucional">Institucional</a></h2>

                        <?php
                            query_posts( array(
                               //'posts_per_page' => 3,
                               'cat' => '1',
                               'orderby'=> 'date&order=ASC',
                               'post__not_in' => array($post_ID)
                             ));
                        ?>
                        <?php 
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();
                        ?>

                            <div class="post" id="post-<?php echo get_the_ID(); ?>">
                                <a href="<?php the_permalink(); ?>">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="thumb">
                                        <?php the_post_thumbnail( 'medium' ); ?>
                                    </div>
                                    <div class="content">
                                            <?php the_excerpt(); ?>
                                    </div>
                                    <div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
                                </a>
                            </div>

                        <?php
                                endwhile;
                                wp_reset_query();
                                // // Previous/next post navigation.
                                // twentyfourteen_paging_nav();
                            else :
                        ?>
                            <p>Nenhum Notícia foi encontrata</p>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
?>
