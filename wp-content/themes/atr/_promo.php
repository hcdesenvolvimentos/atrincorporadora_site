<?php
/*
	Template Name: Promoção
*/

get_header(); ?>

    <div id="dad">
        <!-- Page Content -->
        <div class="container white-area-padding">
            <div class="row row-area">
                <div class="col-md-6 interesse">
                    <div class="clear-fix">
                        <h2>Promoção:</h2>
                        <?php echo do_shortcode( '[contact-form-7 id="253" title="Promoção"]' ); ?>
                        <span class="left">*Nosso tempo médio de respostas é de 24hs.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
