<?php
/*
Plugin Name: Klickpages
Description: Este plugin permite que você utilize uma página criada no Klickpages em seu site Wordpress ou Klicksite.
Version: 2.0.7
Author: Klickpages
Author URI: http://klickpages.com.br/
*/
/**
 * Define here your plugin configurations
 */
define('PLUGIN_NAME', 'klickpages');
define('PLUGIN_VERSION', '2.0.7');
define('PLUGIN_ENV', 'production');
define('PLUGIN_IS_KLICKSITE', false);

$models = array('Page', 'ApiKey', 'App');
$libs   = array('Alert', 'Klickart');

/**
 * Don't edit after this line
 */
session_start();

define('PLUGIN_DIR', dirname(__FILE__));
define('PLUGIN_APP_PATH', PLUGIN_DIR . '//app/');

include 'helpers.php';

include 'bootstrap/security.php';
include 'bootstrap/setup.php';
include 'bootstrap/assets.php';

load_libs($libs);
load_models($models);

include 'bootstrap/padmin.php';
include 'bootstrap/public.php';

/**
 * Load models
 * @param  array $models
 * @return void
 */
function load_models($models) {
  foreach ($models as $model) {
    include 'app/models/' . $model . '.php';
  }
}

/**
 * Load libs
 * @param  array $libs
 * @return void
 */
function load_libs($libs) {
  foreach ($libs as $lib) {
    include 'libs/' . $lib . '.php';
  }
}
