(function($) {
  $(document).ready(function() {
    /* Load tables */
    var Request = window.kp_request,
        Table = window.kp_table;

    Request.perform('GET', 'pages').then(function(pages) {
      loadTable('#kp-table-all', pages);
      loadTable('#kp-table-imported');
      $.kpModal('load');
    }, requestFallback);

    function requestFallback(xhr) {
      if(xhr.status === 401) {
        $('#kp-alert-apikey').kpAlert('on');
      }
    }

    function loadTable(selector, pages) {
      var table = new Table(selector + ' .kp-table'),
          $searchInput = $(selector + ' .kp-search input');

      if(pages) {
        pages.forEach(function(page) {
          table.addRow(page, 'name');
        });
      }

      table.tablesorter();
      table.addSearchField($searchInput);
    }

    /* Dynamic input forms */
    $('select[name="option"]').on('change', function() {
      switch($(this).val()) {
        case 'new':
          showInputsForNew();
          break;
        case 'home':
          showInputsForHome();
          break;
        case 'existent':
          showInputsForExistent();
          break;
      }
    });

    function showInputsForNew() {
      $('#kp-import-form-wp-pages').addClass('kp-hide');
      addName();
    }

    function showInputsForHome() {
      $('#kp-import-form-wp-pages').addClass('kp-hide');
      removeName();
    }

    function showInputsForExistent() {
      $('#kp-import-form-wp-pages').removeClass('kp-hide');
      removeName();
    }

    function removeName() {
      var element = $('#kp-import-form-name');
      element.addClass('kp-hide');
      element.find('input').removeAttr('required');
    }

    function addName() {
      var element = $('#kp-import-form-name');
      element.removeClass('kp-hide');
      element.find('input').attr('required', 'required');
    }
  });
})(jQuery);
