(function($) {
  $(document).ready(function() {
    var Table = function(selector) {
      this.table = $(selector);

      this.addRow = function(data, attr) {
        var $tbody = this.table.find('tbody'),
            $row = $('<tr>'),
            $item = $('<td>'),
            $options = $('<td class="kp-options"></td>');

        $item.html(data[attr]);

        $options.append(this.getImportOpt(data));

        $row.append($item);
        $row.append($options);
        $tbody.append($row);
      };

      this.getImportOpt = function(data) {

        var img_path = $('meta[name="plugin_asset"]'),
            img = $('<img src="' + img_path.attr('content') + 'img/kp-select.png" alt="Ícone de selecionar" />'),
            opt = $('<button title="Importar" data-toggle="modal" data-target="#kp-modal-import" class="kp-btn-simple"></button>');

        opt.append(img);
        opt.attr('data-share', JSON.stringify(data));

        return opt;
      };

      this.tablesorter = function() {
        if(this.table.find('tbody tr').length > 0) {
          this.table.tablesorter({sortList: [[0,0]]});
        }
      };

      this.search = function(query) {
        var rows = this.table.find('tbody tr');

        query = query.toLowerCase();

        [].forEach.call(rows, function(row) {
          var $row = $(row),
              content = $row.find('td').text().toLowerCase();

          content.indexOf(query) > -1 ? $row.removeClass('kp-hide') : $row.addClass('kp-hide');
        });
      };

      this.addSearchField = function($field) {
        var self = this;

        $field.on('keyup', function() {
          var query = $(this).val();
          self.search(query);
        });
      };
    };

    window.kp_table = Table;
  });
})(jQuery);
