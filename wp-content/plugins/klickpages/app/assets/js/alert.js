(function($) {
  $(document).ready(function() {

    $.fn.kpAlert = function(action) {
      switch(action) {
        case 'on':
          this.removeClass('kp-alert-off');
          break;
        case 'off':
          this.addClass('kp-alert-off');
          break;
      }
    };

    $('[data-dismiss="alert"]').on('click', function() {
      $(this).parents('.kp-alert').kpAlert('off');
    });
  });
})(jQuery);

