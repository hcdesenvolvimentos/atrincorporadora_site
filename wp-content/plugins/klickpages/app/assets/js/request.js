(function($) {
  $(document).ready(function() {
    var Request = {
      isSetup: false,
      setup: function() {
        var tokenValue = $('input[name="apikey"]').val();

        this.url = $('[name="klickart_url"]').attr('content');

        $.ajaxSetup({
          dataType: 'json',
          headers: {'X-API-KEY': tokenValue}
        });

        this.setup = true;
      },
      perform: function(type, path) {
        if(!Request.isSetup) {
          Request.setup();
        }

       return $.ajax({
          type: type,
          url: Request.url + '/api/' + path
        });
      }
    };

    window.kp_request = Request;
  });
})(jQuery);
