<div class="kp-wrapper">
  <?php include 'alerts.php' ?>
  <header class="kp-header">
    <img src="<?php echo plugin_asset('img/kp-logo.png') ?>" alt="Logo do Klickpages">
  </header>
  <div class="kp-container">
    <section id="kp-apikey" class="kp-clearfix kp-row">
      <div class="kp-col-6">
        <h2 class="kp-title">Instruções</h2>
        <p>
          Para utilizar este plugin, você irá precisar informar a sua chave de acesso no campo de API Key e salvar, feito isso e tendo o seu código validado, você poderá escolher entre as suas páginas criadas no Klickpages e transformá-las em uma página normal do seu site ou em sua página inicial.
        </p>
      </div>
      <div class="kp-col-6">
        <form action="<?php echo plugin_url('ApiKey', 'update') ?>" id="kp-apikey-form" method="POST">
          <label for="">Insira abaixo a sua API Key:</label>
          <div class="kp-input-group kp-clearfix">
            <input type="text" placeholder="Insira sua API Key aqui" name="apikey" value="<?php echo $apikey ?>" required>
            <button type="submit">ok</button>
          </div>
        </form>
      </div>
    </section> <!-- /#kp-apikey -->
    <section id="kp-pages">
      <div class="kp-buttons">
        <button class="kp-btn kp-btn-outline" data-toggle="tab" data-target="#kp-table-imported">Já importadas</button>
        <button class="kp-btn kp-btn-outline active" data-toggle="tab" data-target="#kp-table-all">Klickpages</button>
      </div>
      <?php include 'table_kp_pages.php' ?>
      <?php include 'table_imported_pages.php' ?>
    </section><!-- /#kp-pages -->
  </div>
</div>

<?php include 'modal_import.php' ?>
<?php include 'modal_delete.php' ?>