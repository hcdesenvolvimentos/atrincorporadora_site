<div class="kp-modal" id="kp-modal-import">
  <div class="kp-modal-header">Importar página</div>
  <div class="kp-modal-body">
    <h2 class="kp-title" data-bind="name"></h2>
    <form action="<?php echo plugin_url('pages', 'import') ?>" method="POST">
      <div class="kp-form-group">
        <select class="kp-form-control" name="option">
          <option value="new">Nova página</option>
          <option value="home">Página Inicial</option>
          <option value="existent">Página Existente</option>
        </select>
      </div>
      <div class="kp-form-group kp-hide" id="kp-import-form-wp-pages">
        <select class="kp-form-control" name="wp_page" placeholder="Selecione uma página">
          <option value="" disabled selected>Selecione uma página</option>
          <?php foreach($wpPages as $wpPage): ?>
            <option value="<?php echo $wpPage->ID ?>"><?php echo $wpPage->post_title ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="kp-form-group" id="kp-import-form-name">
        <input required name="name" type="text" class="kp-form-control" placeholder="Digite o nome da página...">
      </div>
      <div class="kp-form-group kp-clearfix">
        <input type="hidden" name="slug" data-bind="slug">
        <input type="hidden" name="kp_name" data-bind="name">
        <input type="hidden" name="domain" data-bind="domain">
        <input type="hidden" name="optional_path" data-bind="optional_path">
        <button class="kp-btn kp-btn-primary kp-pull-right" type="submit">Importar</button>
      </div>
    </form>
  </div>
</div>