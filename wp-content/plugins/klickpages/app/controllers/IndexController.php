<?php

class IndexController {
  /**
   * Show index page
   *
   * @return boolean
   */
  function index() {
    $apikey        = ApiKey::get();
    $importedPages = Page::all();
    $wpPages       = get_pages();

    return plugin_view('index', compact('apikey', 'importedPages', 'wpPages'));
  }
}