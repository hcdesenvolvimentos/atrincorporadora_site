<?php

class ApiKeyController {
  /**
   * Update apikey
   *
   * @return boolean
   */
  function update() {
    ApiKey::update($_POST['apikey']);

    kp_success('<b>API Key</b> salva com sucesso!');

    return plugin_redirect('index', 'index');
  }
}