<?php

class Page {
  /**
   * Store option key
   *
   * @var string
   */
  protected static $store_option = 'klickpages_imported_pages';

  /**
   * Get all
   *
   * @return array
   */
  static function all() {
    return get_option(self::$store_option, array());
  }

  /**
   * Update page
   *
   * @param  int $id
   * @param  array $data
   * @return void
   */
  static function update($id, $data) {
    $pages = self::all();
    $pages[$id] = $data;

    self::update_all($pages);
  }

  /**
   * Update all store option
   *
   * @param  array $data
   * @return boolean
   */
  static function update_all($data) {
    return update_option(self::$store_option, $data);
  }

  /**
   * Find by id
   *
   * @param  int $id
   * @return array|boolean
   */
  static function find($id) {
    $pages = self::all();

    if(isset($pages[$id])) {
      return $pages[$id];
    }

    return false;
  }

  /**
   * Delete a register
   *
   * @param  int $id [description]
   * @return boolean
   */
  static function delete($id) {
    $pages = self::all();

    unset($pages[$id]);

    return self::update_all($pages);
  }

  /**
   * Create an register
   *
   * @param  int    $id
   * @param  array  $data
   * @return boolean
   */
  static function create($id, $data) {
    $pages = self::all();
    $pages[$id] = $data;

    return self::update_all($pages);
  }

  /**
   * Create multiple registers
   *
   * @param  array  $new_pages
   * @return void
   */
  static function create_all($new_pages) {
    $pages = self::all();
    $updated_pages = array_merge($pages, $new_pages);

    self::update_all($new_pages);
  }

  /**
   * Create the homepage
   *
   * @param  int    $id
   * @param  array  $data
   * @return boolean
   */
  static function create_home($id, $data) {
    $pages = self::all();
    $home = self::find_home();

    if($home !== false) {
      self::delete($home);
    }

    return self::create($id, $data);
  }

  /**
   * Find the homepage
   *
   * @return int|boolean
   */
  static function find_home() {
    $pages = self::all();

    foreach ($pages as $id => $page) {
      if($page['type'] == 'home') {
        return $id;
      }
    }

    return false;
  }
}
