<?php

class ApiKey {
  /**
   * Store option key
   *
   * @var string
   */
  protected static $store_option = 'klickpages_apikey';

  /**
   * Get value from store option
   *
   * @return string
   */
  static function get() {
    return get_option(self::$store_option);
  }

  /**
   * Update store option
   *
   * @param  string $value
   * @return boolean
   */
  static function update($value) {
    if (!empty($value))
      return update_option(self::$store_option, $value);

    return false;
  }
}