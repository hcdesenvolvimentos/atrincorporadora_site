<?php

add_action('admin_init','plugin_setup_assets');
add_action('admin_head','plugin_setup_metas');

/**
 * Setup styles and javascript assets
 *
 * @return void
 */
function plugin_setup_assets() {
  if(isset($_GET['page']) && $_GET['page'] == PLUGIN_NAME) {
    plugin_setup_styles();
    plugin_setup_scripts();
  }
}

/**
 * Setup styles
 *
 * @return void
 */
function plugin_setup_styles() {
  wp_register_style('kp_style', plugin_asset('css/style.css'));

  wp_enqueue_style('kp_style');
}

/**
 * Setup Scripts
 *
 * @return void
 */
function plugin_setup_scripts() {
  wp_register_script('kp_jquery',  plugin_asset('vendor/jquery/jquery.min.js'));
  wp_register_script('jquery.tablesorter',  plugin_asset('vendor/jquery.tablesorter/jquery.tablesorter.min.js'));
  wp_register_script('kp_request',  plugin_asset('js/request.js'));
  wp_register_script('kp_alert',    plugin_asset('js/alert.js'));
  wp_register_script('kp_tabs',     plugin_asset('js/tabs.js'));
  wp_register_script('kp_modal',    plugin_asset('js/modal.js'));
  wp_register_script('kp_table',    plugin_asset('js/table.js'));
  wp_register_script('kp_script',   plugin_asset('js/script.js'));

  wp_enqueue_script('kp_jquery');
  wp_enqueue_script('jquery.tablesorter');
  wp_enqueue_script('kp_request');
  wp_enqueue_script('kp_alert');
  wp_enqueue_script('kp_tabs');
  wp_enqueue_script('kp_modal');
  wp_enqueue_script('kp_table');
  wp_enqueue_script('kp_script');
}

/**
 * Setup meta tags
 *
 * @return void
 */
function plugin_setup_metas() {
  echo '<meta name="klickart_url" content="' . App::getKlickartUrl() . '">';
  echo '<meta name="plugin_asset" content="' . plugin_asset('') . '">';
}