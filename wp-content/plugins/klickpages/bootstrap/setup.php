<?php

register_activation_hook(__FILE__, 'plugin_setup');

if(PLUGIN_IS_KLICKSITE) {
  add_action( 'admin_init', 'klicksite_setup' );
}

/**
 * Execute actions whem the plugin is installed
 *
 * @return void
 */
function plugin_setup() {
  add_option('klickpages_apikey', null);
  add_option('klickpages_imported_pages', array());
  migrate_old_data();
}

/**
 * Migrate old plugin data
 *
 * @return void
 */
function migrate_old_data() {
  ApiKey::update(get_option('kpapikey'));

  $old_pages = get_option('kppages');
  $parsed_pages = parse_pages($old_pages);

  Page::create_all($parsed_pages);
}

/**
 * Setup for Klicksite
 *
 * @return void
 */
function klicksite_setup() {
  $updated = get_option('plugin_updated');

  if($updated !== PLUGIN_VERSION) {
    plugin_setup();
  }

  update_option('plugin_updated', PLUGIN_VERSION);
}

/**
 * Parse old pages
 *
 * @param  array $old_pages
 * @return array
 */
function parse_pages($old_pages) {
  $parsed_pages = array();

  foreach ($old_pages as $key => $old_page) {
    $id = $old_page['idwp'];

    $parsed_pages[$id] = array(
      'name'          => $old_page['name'],
      'kp_name'       => $old_page['pagename'],
      'slug'          => $old_page['slug'],
      'domain'        => null, //Will be update when access at first time
      'optional_path' => null, //Will be update when access at first time
      'type'          => $key === 'paginainicial' ? 'home' : 'page'
    );
  }

  return $parsed_pages;
}